# Installation

Clone the repository (or download and extract it) and add the destination folder to your plugins in GIMP, via Edit -> Settings -> Folders -> Plugins. Then restart GIMP

## For Windows Users

You need to add sitecustomize.py to the following directories

C:\Program Files\GIMP 2\lib\python2.7

C:\Program Files\GIMP 2\32\lib\python2.7

If the directories do not exists you can simply create them