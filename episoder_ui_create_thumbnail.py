from gimpfu import *
from episoder import general as episoder
from episoder import io as episoder_io

def create_thumbnails(image, name, digits, epstart, epend, alpharange, bgcolor, radius):
    if name == "":
        name = str.split(image.name, '.')[0]
    filepath = episoder_io.init_home(image)
    for i in range(epstart, epend+1):
        episoder.set_episode(image, i, digits, alpharange, bgcolor, radius)
        filename = '%s/%s-%03d.png' % (filepath, name, i, )
        episoder_io.save_image(image, filename)

register(
    "ima_episoder_ui_create_thumbnail",
    'Create thumbnails from your image template',
    'You need a layer group called NUMBER-GROUP with sublayers EP-NUMBER and EP-COLOUR',
    "Tim Otlik",
    "Tim Otlik 2020. MIT License",
    "2020",
    "Create Thumbnails",
    "",
    [
        (PF_IMAGE, "image", "Input image", None),
        (PF_STRING, "name", "Use this as export name", ""),
        (PF_INT32, "digits", "Amount of digits (e.g. 2 for 01, 3 for 001)", 3),
        (PF_INT32, "epstart", "First number to generate", 0),
        (PF_INT32, "epend", "Last number to generate", 20),
        (PF_INT32, "alpharange", "Increase Alpha Selection by pixel", 3),
        (PF_COLOR, "bgcolour", "Number Background Color", (1.0, 1.0, 1.0)),
        (PF_FLOAT, "radius", "Gaussian Blur Radius", 1.5),
    ],
    [],
    create_thumbnails,
    menu="<Image>/Filters/IMA/Episoder",
)
main()