from gimpfu import *
from episoder import general as episoder
from episoder import io as episoder_io

def episoder_ui_inc_episode(image, digits, alpharange, bgcolor, radius):
    number_layer, colour_layer = episoder.search_layers(image)
    number = pdb.gimp_text_layer_get_text(number_layer)
    try:
       number = int(number) + 1
    except:
        number = 0
    episoder.set_episode(image, number, digits, alpharange, bgcolor, radius)

register(
    "ima_episode_ui_inc_episode",
    'Increase episode number in your template',
    'You need a layer group called NUMBER-GROUP with sublayers EP-NUMBER and EP-COLOUR',
    "Tim Otlik",
    "Tim Otlik 2020. MIT License",
    "2020",
    "Increase Episode Number",
    "",
    [
        (PF_IMAGE, "image", "Input image", None),
        (PF_INT32, "digits", "Amount of digits (e.g. 2 for 01, 3 for 001)", 3),
        (PF_INT32, "alpharange", "Increase Alpha Selection by pixel", 3),
        (PF_COLOR, "bgcolour", "Number Background Color", (1.0, 1.0, 1.0)),
        (PF_FLOAT, "radius", "Gaussian Blur Radius", 1.5),
    ],
    [],
    episoder_ui_inc_episode,
    menu="<Image>/Filters/IMA/Episoder",
)
main()
