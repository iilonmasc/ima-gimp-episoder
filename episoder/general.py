from gimpfu import *

def get_font(layer):
    font = pdb.gimp_text_layer_get_font(layer)
    font_size, font_unit = pdb.gimp_text_layer_get_font_size(layer)
    return font, font_size, font_unit

def search_layers(image):
    for image_layer in image.layers:
        if type(image_layer) == gimp.GroupLayer and image_layer.name == "NUMBER-GROUP":
            group_layer = image_layer
    for layer in group_layer.layers:
        if layer.name == "EP-NUMBER":
            number_layer = layer
        if layer.name == "EP-COLOUR":
            colour_layer = layer
    return number_layer, colour_layer

def set_episode(image, number, digits, alpharange, bgcolor, radius):
    current_foreground=pdb.gimp_context_get_foreground()
    number_layer, colour_layer = search_layers(image)
    font, font_size, font_unit = get_font(number_layer)
    pdb.gimp_text_layer_set_text(number_layer, "%0{}d".format(digits) % (number,))
    pdb.gimp_text_layer_set_font(number_layer, font)
    pdb.gimp_text_layer_set_font_size(number_layer, font_size, font_unit)
    pdb.gimp_edit_clear(colour_layer)
    pdb.gimp_image_select_item(image, 0, number_layer)
    pdb.gimp_selection_grow(image, alpharange)
    pdb.gimp_image_set_active_layer(image, colour_layer)
    pdb.gimp_context_set_foreground(bgcolor)
    pdb.gimp_drawable_edit_fill(colour_layer, 0)
    pdb.gimp_selection_none(image)
    pdb.plug_in_gauss(image, colour_layer, radius, radius, 0)
    pdb.gimp_context_set_foreground(current_foreground)