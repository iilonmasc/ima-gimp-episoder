import os
from gimpfu import *


def init_home(image):
    home = os.path.expanduser("~")
    gimp_home = home + '/' + 'GIMP/'
    episoder_home = gimp_home + 'Episoder/'
    image_home = episoder_home + str.split(image.name, '.')[0]
    if not os.path.exists(gimp_home):
        os.mkdir(gimp_home)
    if not os.path.exists(episoder_home):
        os.mkdir(episoder_home)
    if not os.path.exists(image_home):
        os.mkdir(image_home)
    return image_home

def save_image(image, filename):
    new_image = pdb.gimp_image_duplicate(image)
    layer = pdb.gimp_image_merge_visible_layers(new_image, CLIP_TO_IMAGE)
    pdb.gimp_file_save(new_image, layer, filename, '?')
    pdb.gimp_image_delete(new_image)

